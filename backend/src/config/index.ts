export default () => ({
  port: parseInt(process.env.PORT),
  db_url: String(process.env.DB_URL),
  environment: String(process.env.NODE_ENV),
});
