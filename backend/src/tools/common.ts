export enum ENV {
  PORT,
  DB_URL,
  NODE_ENV,
}
