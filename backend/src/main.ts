// Core
import { NestFactory } from '@nestjs/core';
import { Logger, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

// Modules
import { AppModule } from './app.module';

// Tools
import { ENV } from './tools';

(async () => {
  const app = await NestFactory.create(AppModule);
  app.enableCors({
    methods: ['GET', 'POST'],
    origin: true
  });


  const configService = app.get<ConfigService>(ConfigService);
  const port = configService.get(ENV[ENV.PORT]) ?? 3001;

  app.useGlobalPipes(new ValidationPipe());

  await app.listen(port);

  Logger.log(`Server running at http://localhost:${port}`, 'NestApplication');
})();
