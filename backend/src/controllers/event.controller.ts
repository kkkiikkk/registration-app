// Core
import { Get, Post, Query, Controller, Param, HttpCode, HttpStatus, Body } from '@nestjs/common';

// Services
import { EventService } from '../services/event/event.service';
import { ParticipantService } from '../services/participant/participant.service';

// Models
import { Event } from '../db/models/event.model';
import { Participant } from '../db/models/participant.model'

// Interfaces
import { IPagination } from '../core/interfaces';

// Tools
import { CreateParticipantDto } from '../core/dtos/participant.dto';
import { EventQueryDto } from '../core/dtos/event.dto';

@Controller('api/events')
export class EventController {
  constructor(
      private readonly eventService: EventService,
      private readonly participantService: ParticipantService
  ) {}

  @Get()
  @HttpCode(HttpStatus.OK)
  async findAll(@Query() query: EventQueryDto): Promise<IPagination<Event>> {
    return await this.eventService.findAll(query);
  }

  @Get(':id')
  @HttpCode(HttpStatus.OK)
  async findById(@Param('id') id: string): Promise<Event> {
    return await this.eventService.findById(id);
  }

  @Post(':id/participants')
  @HttpCode(HttpStatus.CREATED)
  async createParticipant(
      @Body() payload: CreateParticipantDto,
      @Param('id') id: string
  ): Promise<Participant> {
      return await this.participantService.create(payload, id)
  }

  @Get(':id/participants')
  @HttpCode(HttpStatus.OK)
  async findParticipantByEvent(@Param('id') id: string): Promise<Participant[]> {
    return await this.participantService.findParticipantsByEventId(id);
  }
}
