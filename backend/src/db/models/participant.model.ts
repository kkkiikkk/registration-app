// Core
import {Table, DataType, Column, Model, ForeignKey, BelongsTo } from 'sequelize-typescript'
import { v4 as uuid } from 'uuid';

// Models
import { Event } from './event.model';

export enum FindOutType {
    SocialMedia = 'SocialMedia',
    Friends = 'Friends',
    Myself = 'Myself'
}

@Table({
    defaultScope: {
        attributes: {
            exclude: ['dateOfBirth', 'findOutType', 'createdAt', 'updatedAt']
        }
    }
})
export class Participant extends Model {
    @Column({ primaryKey: true, defaultValue: () => uuid(), type: DataType.STRING })
    id: string;

    @Column({ type: DataType.STRING, allowNull: false })
    fullName: string;

    @Column({ type: DataType.STRING, allowNull: false })
    email: string;

    @Column({ type: DataType.DATE, allowNull: false })
    dateOfBirth: Date;

    @Column({ allowNull: false, type: DataType.ENUM(...Object.values(FindOutType)) })
    findOutType: FindOutType;

    @ForeignKey(() => Event)
    @Column({ type: DataType.STRING, allowNull: false })
    eventId: string;

    @BelongsTo(() => Event, 'eventId')
    event: Event
}
