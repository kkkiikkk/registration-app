// Core
import { Column, Model, Table, DataType, HasMany } from 'sequelize-typescript';
import { v4 as uuid } from 'uuid';

// Models
import { Participant } from './participant.model';

@Table({
  defaultScope: {
    attributes: {
      exclude: ['createdAt', 'updatedAt']
    }
  }
})
export class Event extends Model {
  @Column({
    primaryKey: true,
    type: DataType.STRING ,
    defaultValue: () => uuid(),
  })
  id: string;

  @Column({ allowNull: false })
  title: string;

  @Column({ type: DataType.TEXT, allowNull: false })
  description: string;

  @Column({ type: DataType.DATE, allowNull: false })
  eventDate: Date;

  @Column({ allowNull: false })
  organaizer: string;

  @HasMany(() => Participant)
  participants: Participant[];
}
