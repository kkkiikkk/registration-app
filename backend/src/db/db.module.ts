// Core
import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { ConfigModule, ConfigService } from '@nestjs/config';

// Tools
import { ENV } from '../tools';

// Models
import { Event } from './models/event.model';
import { Participant } from './models/participant.model';

@Module({
  imports: [
    SequelizeModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get(ENV[ENV.DB_URL]),
        models: [Event, Participant],
        dialect: 'postgres',
        logging: configService.get(ENV[ENV.NODE_ENV]) === 'development',
        autoLoadModels: true,
      }),
      inject: [ConfigService],
    }),
  ],
})
export class DbModule {}
