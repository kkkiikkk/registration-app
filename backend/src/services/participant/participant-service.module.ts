// Core
import { Module, forwardRef } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

// Services
import { ParticipantService } from './participant.service';

// Models
import { Event } from '../../db/models/event.model';
import { Participant } from '../../db/models/participant.model';

// Controllers
import { EventController } from '../../controllers/event.controller';

// Modules
import { EventModule } from '../event/event-service.module'

@Module({
    imports: [SequelizeModule.forFeature([Event, Participant]), forwardRef(() => EventModule)],
    providers: [ParticipantService],
    exports: [ParticipantService],
    controllers: [EventController],
})
export class ParticipantModule {}
