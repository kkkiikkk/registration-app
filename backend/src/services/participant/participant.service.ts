// Core
import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize'

// Models
import { Event } from '../../db/models/event.model';
import { Participant } from '../../db/models/participant.model';

// Tools
import { CreateParticipantDto } from '../../core/dtos/participant.dto'

@Injectable()
export class ParticipantService {
    constructor(
        @InjectModel(Participant)
        private readonly participantModel: typeof Participant,
        @InjectModel(Event)
        private readonly eventModel: typeof Event
    ) {}

    async create(payload: CreateParticipantDto, eventId: string): Promise<Participant> {
        await this.checkEventById(eventId);

        const participant: Participant = await this.participantModel.findOne({
            where: {
                email: payload.email,
                eventId,
            }
        });

        if (participant) {
            throw new BadRequestException('Participant with such email already exists');
        }

        return await this.participantModel.create({...payload, eventId})
    }

    async findParticipantsByEventId(eventId: string): Promise<Participant[]> {
        await this.checkEventById(eventId);

        return await this.participantModel.findAll({ where: { eventId } })
    }

    private async checkEventById(eventId: string): Promise<void> {
        const event: Event = await this.eventModel.findByPk(eventId);

        if (!event) {
            throw new NotFoundException('Event was not found');
        }
    }
}
