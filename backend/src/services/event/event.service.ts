// Core
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';

// Models
import { Event } from '../../db/models/event.model';

// Interfaces
import { IPagination } from '../../core/interfaces';

// Tools
import { EventQueryDto } from '../../core/dtos/event.dto';

@Injectable()
export class EventService {
  constructor(
    @InjectModel(Event)
    private readonly eventModel: typeof Event,
  ) {}

  async findAll({ offset, limit, sortBy, sortOrder }: EventQueryDto): Promise<IPagination<Event>> {
    const { rows, count } = await this.eventModel.findAndCountAll({
      offset: offset ?? 1,
      limit: limit ?? 2,
      order: [[sortBy ?? 'title', sortOrder ?? 'desc']]
    });
    return { data: rows, count }
  }

  async findById(id: string) {
    const event = await this.eventModel.findByPk(id);

    if (!event) {
      throw new NotFoundException('Event was not found');
    }

    return event;
  }
}
