// Core
import { forwardRef, Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';

// Services
import { EventService } from './event.service';

// Models
import { Event } from '../../db/models/event.model';

// Controllers
import { EventController } from '../../controllers/event.controller';

// Modules
import { ParticipantModule } from '../participant/participant-service.module';

@Module({
  imports: [SequelizeModule.forFeature([Event]), forwardRef(() => ParticipantModule)],
  providers: [EventService],
  exports: [EventService],
  controllers: [EventController],
})
export class EventModule {}
