// Core
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';

// Tools
import config from './config';

// Modules
import { DbModule } from './db/db.module';
import { EventModule } from './services/event/event-service.module';
import { ParticipantModule } from './services/participant/participant-service.module'

// Controllers
import { EventController } from './controllers/event.controller';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [config],
      validationOptions: {
        allowUnknown: true,
        abortEarly: true,
      },
    }),
    DbModule,
    EventModule,
    ParticipantModule,
  ],
  controllers: [EventController],
  providers: [],
})
export class AppModule {}
