// Core
import {IsNotEmpty, IsString, IsEmail, IsDate, IsEnum} from 'class-validator';

// Tools
import { FindOutType } from '../../db/models/participant.model'

export class CreateParticipantDto {
    @IsString()
    @IsNotEmpty()
    fullName: string;

    @IsNotEmpty()
    @IsEmail()
    email: string;

    @IsNotEmpty()
    dateOfBirth: string;

    @IsNotEmpty()
    @IsEnum(FindOutType)
    findOutType: FindOutType;
}
