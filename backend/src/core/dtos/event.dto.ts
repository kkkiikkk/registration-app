// Core
import {IsNumber, IsOptional, IsString} from 'class-validator';
import { Transform } from 'class-transformer';

export class EventQueryDto {
    @Transform(({ value }) => parseInt(value))
    @IsNumber()
    @IsOptional()
    offset: number = 1;

    @Transform(({value}) => parseInt(value))
    @IsNumber()
    @IsOptional()
    limit: number = 10;

    @Transform(({value}) => String(value))
    @IsString()
    @IsOptional()
    sortBy: 'title' | 'eventDate' | 'organaizer' = 'title';

    @Transform(({value}) => String(value))
    @IsString()
    @IsOptional()
    sortOrder: 'asc' | 'desc' = 'desc';
}
