# Events registration-app

## To run backend side

```bash
cd backend
npm ci
// For dev environment
npm run start:dev
// For production mode
npm run start:prod
```

### Env example backend

```env
PORT=4000
DB_URL=postgres://username:password@127.0.0.1:5432/db-name
```

## To run client side

```bash
cd client
npm ci
// For dev environment
npm run start
// For production mode
npm run build
npm run serve
```

### Env example backend

```env
APP_NAME=EXAMPLE
PUBLIC_URL=http://localhost:3000
API_URL=http://localhost:4000
```
