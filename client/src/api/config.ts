// Init
import { API_URL } from '../init'

type EVENT_BY_PAGINATION = {
    limit: number;
    page: number;
    sortBy: 'title' | 'eventDate' | 'organaizer';
    sortOrder: 'asc' | 'desc'
}


export const API = {
    EVENTS: {
        EVENTS_BY_PAGINATION: ({ limit, sortBy, sortOrder, page }: EVENT_BY_PAGINATION) => `${API_URL}/api/events?offset=${page}&limit=${limit}&sortBy=${sortBy}&sortOrder=${sortOrder}`,
        PARTICIPANTS: {
            PARTICIPANTS_BY_EVENT: (id: string) => `${API_URL}/api/events/${id}/participants`,
            ADD_PARTICIPANT: (id: string) => `${API_URL}/api/events/${id}/participants`,

        }
    },
}

export const HEADERS = {
    'Content-Type': 'application/json',
};
