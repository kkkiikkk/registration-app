// Config of API
import { API, HEADERS } from './config';

// Types
import * as types from '../bus/participants/saga/types';

export const addParticipantsFetcher = (id: string, payload: types.FetchAddParticipantsRequest['body']) => {
    return fetch(API.EVENTS.PARTICIPANTS.ADD_PARTICIPANT(id), {
        method: 'POST',
        body: JSON.stringify(payload),
        headers: {
            ...HEADERS
        }
    })
}

export const participantsByEvent = (id: string) => {
    return fetch(API.EVENTS.PARTICIPANTS.PARTICIPANTS_BY_EVENT(id), {
        method: 'GET',
        headers: {
            ...HEADERS
        }
    })
}
