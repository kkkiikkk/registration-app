// Config of API
import { API, HEADERS } from './config';

// Types
import * as types from '../bus/events/saga/types';

export const eventsByPaginationFetcher = (payload: types.FetchEventsPaginationRequest) => {
    return fetch(API.EVENTS.EVENTS_BY_PAGINATION(payload), {
        method: 'GET',
        headers: {
            ...HEADERS
        }
    })
}

