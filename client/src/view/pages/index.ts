// Core
import { lazy } from 'react';

// pages
export const Events = lazy(() => import('./Events'));
export const RegisterEvent = lazy(() => import('./RegisterEvent'));
export const ParticipantsByEvents = lazy(() => import('./ParticipantsByEvent'));
