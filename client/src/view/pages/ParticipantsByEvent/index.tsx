// Core
import React, { FC, useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';

// Styles
import * as S from './styles';

// Components
import { ErrorBoundary } from '../../components';

// Hooks
import { useEvents } from '../../../bus/events';
import { useParticipants } from '../../../bus/participants';

const Participants: FC = () => {
    const { currentEvent } = useEvents();
    const { fetchParticipantsByEvent, participants } = useParticipants();
    const params = useParams();
    const [searchQuery, setSearchQuery] = useState('');

    useEffect(() => {
        fetchParticipantsByEvent({ id: params.id as string });
    }, [params.id]);

    const handleSearchChange = (e) => {
        setSearchQuery(e.target.value);
    };

    const filteredParticipants = participants?.filter(participant =>
        participant.fullName.toLowerCase().includes(searchQuery.toLowerCase()) ||
        participant.email.toLowerCase().includes(searchQuery.toLowerCase())
    );

    return (
        <section>
            <S.EventTitle>{ currentEvent?.title }</S.EventTitle>
            <S.SearchInput
                type="text"
                placeholder="Search participants by name or email..."
                value={searchQuery}
                onChange={handleSearchChange}
            />
            <S.Container>
                {filteredParticipants?.map(participant => (
                    <S.Card key={participant.id}>
                        <S.CardContent>
                            <S.ParticipantName>{participant.fullName}</S.ParticipantName>
                            <S.ParticipantEmail>{participant.email}</S.ParticipantEmail>
                        </S.CardContent>
                    </S.Card>
                ))}
            </S.Container>
        </section>
    );
}

export default () => (
    <ErrorBoundary>
        <Participants />
    </ErrorBoundary>
);
