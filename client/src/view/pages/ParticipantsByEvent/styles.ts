// Core
import styled from 'styled-components';

export const EventTitle = styled.h1`
  text-align: center;
  margin-bottom: 16px;
`;

export const Container = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(250px, 1fr));
  gap: 16px;
  padding: 16px;
  background-color: #f8f9fa;
`;

export const Card = styled.div`
  border: 1px solid #ccc;
  border-radius: 8px;
  background-color: #fff;
  padding: 16px;
  box-shadow: 0 2px 4px rgba(0, 0, 0, 0.1);
`;

export const CardContent = styled.div`
  display: flex;
  flex-direction: column;
`;

export const ParticipantName = styled.h2`
  font-size: 18px;
  margin: 0 0 8px;
`;

export const ParticipantEmail = styled.p`
  font-size: 16px;
  margin: 0;
  color: #555;
`;

export const SearchInput = styled.input`
  display: block;
  width: calc(100% - 32px);
  margin: 0 auto 16px;
  padding: 8px;
  font-size: 16px;
  border: 1px solid #ccc;
  border-radius: 4px;
  background-color: #fff;
`;
