// Core
import React, { FC, useState } from 'react';
import { useNavigate } from 'react-router-dom'
import { useParams  } from 'react-router-dom';

// Components
import { ErrorBoundary } from '../../components';

// Styles
import * as S from './styles';

// Saga
import { useParticipants } from '../../../bus/participants'

export const RegisterEvent: FC = () => {
    const [formData, setFormData] = useState({
        fullName: '',
        email: '',
        dateOfBirth: '',
        findOutType: ''
    });

    const params = useParams();
    const { fetchAddParticipant } = useParticipants();
    const navigate = useNavigate();

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value
        });
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        console.log({ params: { id: params['id'] as string }, body: { ...formData } })
        fetchAddParticipant({ params: { id: params['id'] as string }, body: { ...formData } })
        navigate('/events');
    };

    return (
        <S.FormContainer onSubmit={handleSubmit}>
            <S.FormGroup>
                <label htmlFor="fullName">Full Name</label>
                <input
                    type="text"
                    id="fullName"
                    name="fullName"
                    value={formData.fullName}
                    onChange={handleInputChange}
                    required
                />
            </S.FormGroup>

            <S.FormGroup>
                <label htmlFor="email">Email</label>
                <input
                    type="email"
                    id="email"
                    name="email"
                    value={formData.email}
                    onChange={handleInputChange}
                    required
                />
            </S.FormGroup>

            <S.FormGroup>
                <label htmlFor="dateOfBirth">Date of Birth</label>
                <input
                    type="date"
                    id="dateOfBirth"
                    name="dateOfBirth"
                    value={formData.dateOfBirth}
                    onChange={handleInputChange}
                    required
                />
            </S.FormGroup>

            <S.FormGroup>
                <label>Where did you hear about this event?</label>
                <S.RadioGroup>
                    <label>
                        <input
                            type="radio"
                            name="findOutType"
                            value="SocialMedia"
                            checked={formData.findOutType === 'SocialMedia'}
                            onChange={handleInputChange}
                            required
                        />
                        Social Media
                    </label>
                    <label>
                        <input
                            type="radio"
                            name="findOutType"
                            value="Friends"
                            checked={formData.findOutType === 'Friends'}
                            onChange={handleInputChange}
                        />
                        Friends
                    </label>
                    <label>
                        <input
                            type="radio"
                            name="findOutType"
                            value="Myself"
                            checked={formData.findOutType === 'Myself'}
                            onChange={handleInputChange}
                        />
                        Found Myself
                    </label>
                </S.RadioGroup>
            </S.FormGroup>

            <S.SubmitButton type="submit">Register</S.SubmitButton>
        </S.FormContainer>
    );
}

export default () => (
    <ErrorBoundary>
        <RegisterEvent />
    </ErrorBoundary>
)
