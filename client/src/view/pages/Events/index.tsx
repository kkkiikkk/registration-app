// Core
import React, { FC, useEffect, useState } from 'react';

// Components
import { ErrorBoundary, Event } from '../../components';

// Hooks
import { useEvents } from '../../../bus/events'

// Styles
import * as S from './styles'

// Tools
import { FetchEventsPaginationRequest } from '../../../bus/events/saga/types';
import { initialLimitOfEvents } from '../../../bus/events/slice';
import {setCurrentEvent} from "../../../bus/events/reducers";

const Events: FC = () => {
    const { events, fetchEventsByPagination, setCurrentEvent } = useEvents();
    const [sortCriteria, setSortCriteria] = useState({ sortBy: 'title', sortOrder: 'desc' });
    const [currentPage, setCurrentPage] = useState(0);
    const [totalPages, setTotalPages] = useState( events ? (events.count / initialLimitOfEvents) < 1 ? 1 : events.count / initialLimitOfEvents : 1);

    useEffect(() => {
            fetchEventsByPagination({
                page: 0,
                sortBy: sortCriteria.sortBy as FetchEventsPaginationRequest['sortBy'],
                sortOrder: sortCriteria.sortOrder as FetchEventsPaginationRequest['sortOrder'] ,
                limit: initialLimitOfEvents
            })
    }, [sortCriteria]);

    const handleSortChange = (e) => {
        const value = e.target.value;
        const [sortBy, sortOrder] = value.split('_');
        setSortCriteria({ sortBy, sortOrder });
    };

    const handlePageChange = (newPage: number) => {
        if (newPage >= 0 && newPage < totalPages) {
            setCurrentPage(newPage);
            fetchEventsByPagination({
                page: currentPage,
                sortBy: sortCriteria.sortBy as FetchEventsPaginationRequest['sortBy'],
                sortOrder: sortCriteria.sortOrder as FetchEventsPaginationRequest['sortOrder'] ,
                limit:10
            })
        }
    };

    return (
        <section style={{padding: "7vh 9vh 7vh 9vh"}}>
            <S.SortDropdown onChange={handleSortChange}>
                <option value="title_asc">Title (A-Z)</option>
                <option value="title_desc">Title (Z-A)</option>
                <option value="organaizer_asc">Organizer (A-Z)</option>
                <option value="organaizer_desc">Organizer (Z-A)</option>
                <option value="eventDate_asc">Event Date (Earliest First)</option>
                <option value="eventDate_desc">Event Date (Latest First)</option>
            </S.SortDropdown>
            <S.Container>

                {
                    events?.data.map((event, index) => {
                        return(
                            <Event key={index} setCurrentEvent={() => setCurrentEvent(event)} id={event.id} title={event.title} description={event.description} />
                        )
                    })
                }
            </S.Container>
            <S.Pagination>
                <button onClick={() => handlePageChange(currentPage - 1)} disabled={currentPage === 0}>
                    Previous
                </button>
                <span>Page {currentPage + 1} of {totalPages}</span>
                <button onClick={() => handlePageChange(currentPage + 1)} disabled={currentPage === totalPages - 1}>
                    Next
                </button>
            </S.Pagination>
        </section>

    )
}

export default () => (
    <ErrorBoundary>
        <Events />
    </ErrorBoundary>
)
