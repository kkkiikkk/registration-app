// Core
import React, { FC } from 'react';
import { Link } from 'react-router-dom';

// Styles
import * as S from './styles'

type Props = {
    title: string;
    description: string;
    id: string;
    setCurrentEvent: Function
}

export const Event: FC<Props> = ({ title, description, id, setCurrentEvent }) => {
    return (
        <S.CardContainer>
            <S.Title>{title}</S.Title>
            <S.Description>{description}</S.Description>
            <S.ButtonContainer>
                <S.Button onClick={() => setCurrentEvent()}>
                    <Link to={`/events/${id}/participants`}>
                        View
                    </Link>
                </S.Button>
                <S.Button>
                    <Link to={`/events/${id}/register`}>
                        Register
                    </Link>
                </S.Button>
            </S.ButtonContainer>
        </S.CardContainer>
    )
}
