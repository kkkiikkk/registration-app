// Core
import React, { FC } from 'react';
import { Routes, Route, Navigate } from 'react-router-dom';

// Pages
import * as Pages from '../pages';

// Tools
import * as book from './book';

export const Public: FC = () => {
    return (
        <Routes>
            <Route
                element = { <Pages.Events /> }
                path = { book.EVENTS }
            />
            <Route
                element = { <Pages.RegisterEvent /> }
                path = { book.REGISTER_EVENT }
            />
            <Route
                element = { <Pages.ParticipantsByEvents /> }
                path = { book.PARTICIPANTS_BY_EVENT }
            />
            <Route
                element = {
                    <Navigate
                        replace
                        to = { book.EVENTS }
                    />
                }
                path = '*'
            />
        </Routes>
    );
};
