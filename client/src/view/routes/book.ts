// Books
export const EVENTS = '/events';
export const REGISTER_EVENT = `/events/:id/register`;
export const PARTICIPANTS_BY_EVENT = `/events/:id/participants`
