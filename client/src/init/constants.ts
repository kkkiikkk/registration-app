// Network
export const API_URL = process.env.API_URL || 'http://localhost:3002';

// Local
export const APP_NAME = process.env.APP_NAME || 'app';
