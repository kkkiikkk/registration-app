// Core
import { configureStore } from '@reduxjs/toolkit';

// Middleware
import {
    middleware,
    sagaMiddleware
} from './middleware';

// import rootSaga
import { rootSaga } from './rootSaga'

// Reducers
import events from '../../bus/events/slice'
import participants from '../../bus/participants/slice'

export const store = configureStore({
    reducer: {
        events,
        participants
    },
    middleware: (gDM) => gDM().concat([sagaMiddleware]),
    devTools:   process.env.NODE_ENV !== 'production',
});

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = ReturnType<typeof store.dispatch>

// Run rootSaga
sagaMiddleware.run(rootSaga)
