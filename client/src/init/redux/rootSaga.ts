// Core
import { all } from 'redux-saga/effects';

// Watchers
import { watchEvents } from '../../bus/events/saga'
import { watchParticipants } from '../../bus/participants/saga'

export function* rootSaga() {
    // Uncomment for using
    yield all([
        watchEvents(),
        watchParticipants()
    ]);
}
