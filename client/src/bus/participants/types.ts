// Core
import { CaseReducer, PayloadAction } from '@reduxjs/toolkit';

export type Participant = {
    id: number,
    fullName: string,
    email: string,
    eventId: string
}

export type Participants = Participant[];

export type ParticipantState = {
    currentParticipants: Participants | null
}

// Base contract
export type BaseContact<T = any> = CaseReducer<ParticipantState, PayloadAction<T>>;
