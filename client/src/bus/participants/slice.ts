// Core
import { createSlice } from '@reduxjs/toolkit';

// Types
import * as types from './types';

// Reducers
import * as reducers from './reducers';

export const initialState: types.ParticipantState = {
    currentParticipants: null,
};

export const participantSlice = createSlice({
    name: 'participants',
    initialState,
    reducers,
});

export const sliceName = participantSlice.name;
export const participantsActions = participantSlice.actions;
export default participantSlice.reducer;
