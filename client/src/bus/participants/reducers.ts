// Types
import * as types from './types';

export const setParticipants: types.BaseContact<types.Participants> = (state, action) => ({
    ...state,
    currentParticipants: action.payload
});
