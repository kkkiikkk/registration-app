import {Participants} from "../types";

export type FetchAddParticipantsRequest = {
    body: {
        fullName: string,
        email: string,
        dateOfBirth: string,
        findOutType: string,
    }
    params: {
        id: string
    }

}

export type FetchAddParticipantResponse = {
    id: number,
    fullName: string,
    email: string,
    dateOfBirth: string,
    findOutType: string,
    eventId: string,
    updatedAt: string,
    createdAt: string
}

export type ParticipantsByEventReponse = Participants
