// Core
import { SagaIterator } from '@redux-saga/core';
import { createAction } from '@reduxjs/toolkit';
import {put, takeLatest} from 'redux-saga/effects';

// API
import { addParticipantsFetcher } from '../../../api';

// Slice
import { sliceName } from '../slice';

// Tools
import { makeRequest } from '../../../tools/utils';

// Types
import * as types from './types';

// Action
export const fetchAddParticipantAction = createAction<types.FetchAddParticipantsRequest>(`${sliceName}/FETCH_ADD_PARTICIPANT_ASYNC`)

// Saga
const fetchAddParticipant = (
    callAction: ReturnType<typeof fetchAddParticipantAction>,
) => makeRequest<types.FetchAddParticipantResponse, any>({
    callAction,
    fetchOptions: {
        successStatusCode: 201,
        fetch: () => addParticipantsFetcher(callAction.payload.params.id, callAction.payload.body),
    },
});

// Watcher
export function* watchFetchAddParticipant(): SagaIterator {
    yield takeLatest(fetchAddParticipantAction.type, fetchAddParticipant);
}
