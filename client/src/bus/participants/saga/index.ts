// Core
import { SagaIterator } from '@redux-saga/core';
import { all, call } from 'redux-saga/effects';
import { useCallback } from 'react';

// Tools
import { useDispatch } from '../../../tools/hooks';

// Wathcers & Actions
import { fetchAddParticipantAction, watchFetchAddParticipant } from './fetchAddParticipant';
import { watchFetchParticipantByEvent, fetchParticipantsByEventAction } from './fetchParticipantsByEvent';

// Types
import * as types from './types';

export const useParticipantSaga = () => {
    const dispatch = useDispatch();
    const fetchAddParticipant = useCallback((payload: types.FetchAddParticipantsRequest) => {
        dispatch(fetchAddParticipantAction(payload));
    }, [dispatch]);

    const fetchParticipantsByEvent = useCallback((payload: { id: string }) => {
        dispatch(fetchParticipantsByEventAction(payload));
    }, [dispatch]);
    return {
        fetchAddParticipant,
        fetchParticipantsByEvent
    }
}

export function* watchParticipants(): SagaIterator {
    yield all([
        call(watchFetchAddParticipant),
        call(watchFetchParticipantByEvent)
    ])
}
