// Core
import { SagaIterator } from '@redux-saga/core';
import { createAction } from '@reduxjs/toolkit';
import {put, takeLatest} from 'redux-saga/effects';

// API
import { participantsByEvent } from '../../../api';

// Slice
import { sliceName, participantsActions } from '../slice';

// Tools
import { makeRequest } from '../../../tools/utils';

// Types
import * as types from './types';

// Action
export const fetchParticipantsByEventAction = createAction<{id: string}>(`${sliceName}/FETCH_PARTICIPANTS_BY_EVENT_ASYNC`)

// Saga
const fetchParticipantsByEvent = (
    callAction: ReturnType<typeof fetchParticipantsByEventAction>,
) => makeRequest<types.ParticipantsByEventReponse, any>({
    callAction,
    fetchOptions: {
        successStatusCode: 200,
        fetch: () => participantsByEvent(callAction.payload.id),
    },
    success: function* (result) {
        yield put(participantsActions.setParticipants(result));
    },
});

// Watcher
export function* watchFetchParticipantByEvent(): SagaIterator {
    yield takeLatest(fetchParticipantsByEventAction.type, fetchParticipantsByEvent);
}
