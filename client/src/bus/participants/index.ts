// Tools
import { useDispatch, useSelector } from '../../tools/hooks';

// Slice
import { participantsActions } from './slice';

// Types
import * as types from './types';

// Saga
import { useParticipantSaga } from './saga';

export const useParticipants = () => {
    const eventsSagas = useParticipantSaga();
    const dispatch = useDispatch();
    const participants = useSelector((state) => state.participants.currentParticipants);

    return {
        participants,
        setParticipants: (payload: types.Participants) => dispatch(participantsActions.setParticipants(payload)),
        ...eventsSagas,
    };
};
