// Core
import { SagaIterator } from '@redux-saga/core';
import { all, call } from 'redux-saga/effects';

// Tools
import { useDispatch } from '../../../tools/hooks';

// Wathcers & Actions
import { fetchEventsByPaginationAction, watchFetchEventsByPagination } from './fetchEvents';

// Types
import * as types from './types';

export const useEventsSaga = () => {
    const dispatch = useDispatch();

    return {
        fetchEventsByPagination: (payload: types.FetchEventsPaginationRequest) => dispatch(fetchEventsByPaginationAction(payload))
    }
}

export function* watchEvents(): SagaIterator {
    yield all([
        call(watchFetchEventsByPagination)
    ])
}
