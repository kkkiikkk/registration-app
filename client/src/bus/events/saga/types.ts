// Types
import * as types from '../types';

export type FetchEventsPaginationRequest = {
    limit: number;
    page: number;
    sortBy: 'title' | 'eventDate' | 'organaizer';
    sortOrder: 'asc' | 'desc'
}

export type FetchEventsPaginationResponse = {
    data: types.Event[],
    count: number
}
