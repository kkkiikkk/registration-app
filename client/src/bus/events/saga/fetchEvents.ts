// Core
import { SagaIterator } from '@redux-saga/core';
import { createAction } from '@reduxjs/toolkit';
import {put, takeEvery, takeLatest} from 'redux-saga/effects';

// API
import { eventsByPaginationFetcher } from '../../../api';

// Slice
import { sliceName, eventsActions } from '../slice';

// Tools
import { makeRequest } from '../../../tools/utils';

// Types
import * as types from './types';

// Action
export const fetchEventsByPaginationAction = createAction<types.FetchEventsPaginationRequest>(`${sliceName}/FETCH_EVENTS_BY_PAGINATION_ASYNC`)

// Saga
const fetchEventsByPagination = (
    callAction: ReturnType<typeof fetchEventsByPaginationAction>,
) => makeRequest<types.FetchEventsPaginationResponse, any>({
    callAction,
    fetchOptions: {
        successStatusCode: 200,
        fetch: () => eventsByPaginationFetcher(callAction.payload),
    },
    success: function* (result) {
        yield put(eventsActions.setEvents(result));
    },
});

// Watcher
export function* watchFetchEventsByPagination(): SagaIterator {
    yield takeLatest(fetchEventsByPaginationAction.type, fetchEventsByPagination);
}
