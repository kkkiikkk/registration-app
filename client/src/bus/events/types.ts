// Core
import { CaseReducer, PayloadAction } from '@reduxjs/toolkit';

export type Event = {
    id: string;
    title: string;
    description: string;
    eventDate: string;
    organaizer: string
}

export type Events = {
    count: number;
    data: Event[] | []
};

export type EventState = {
    limit: number;
    page: number;
    events: null | Events;
    currentEvent: null | Event
}

// Base contract
export type BaseContact<T = any> = CaseReducer<EventState, PayloadAction<T>>;
