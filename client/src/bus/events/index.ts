// Tools

import { useDispatch, useSelector } from '../../tools/hooks';

// Slice
import { eventsActions } from './slice';

// Types
import * as types from './types';

// Saga
import { useEventsSaga } from './saga';
import {useEffect} from "react";
import {fetchEventsByPaginationAction} from "./saga/fetchEvents";

export const useEvents = () => {
    const eventsSagas = useEventsSaga();
    const dispatch = useDispatch();
    const events = useSelector((state) => state.events.events);
    const currentEvent = useSelector((state) => state.events.currentEvent);

    return {
        events,
        currentEvent,
        setCurrentEvent:  (
            payload: types.EventState['currentEvent'],
        ) => dispatch(eventsActions.setCurrentEvent(payload)),
        ...eventsSagas,
    };
};
