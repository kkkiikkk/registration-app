// Types
import * as types from './types';

export const setEvents: types.BaseContact<types.Events> = (state, action) => ({
    ...state,
    events: action.payload
})

export const setPageOfEvents: types.BaseContact<types.EventState['page']> = (state, action) => ({
    ...state,
    page: action.payload,
});

export const setCurrentEvent: types.BaseContact<types.EventState['currentEvent']> = (state, action) => ({
    ...state,
    currentEvent: action.payload,
});
