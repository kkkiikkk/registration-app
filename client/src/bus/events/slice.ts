// Core
import { createSlice } from '@reduxjs/toolkit';

// Types
import * as types from './types';

// Reducers
import * as reducers from './reducers';

export const initialLimitOfEvents = 10;
export const initialPageOfEvents = 1;

export const initialState: types.EventState = {
    events: null,
    currentEvent: null,
    limit: initialLimitOfEvents,
    page: initialPageOfEvents
};

export const eventSlice = createSlice({
    name: 'events',
    initialState,
    reducers,
});

export const sliceName = eventSlice.name;
export const eventsActions = eventSlice.actions;
export default eventSlice.reducer;
